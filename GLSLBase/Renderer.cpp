#include "stdafx.h"
#include "Renderer.h"
#include "LoadPng.h"
#include <Windows.h>
#include <cstdlib>
#include <cassert>

float g_Time = 0;


Renderer::Renderer(int windowSizeX, int windowSizeY)
{
	Initialize(windowSizeX, windowSizeY);
}


Renderer::~Renderer()
{
}

void Renderer::Initialize(int windowSizeX, int windowSizeY)
{
	//Set window size
	m_WindowSizeX = windowSizeX;
	m_WindowSizeY = windowSizeY;

	//Load shaders
	m_Shader_SolidRect = CompileShaders("./Shaders/SolidRect.vs", "./Shaders/SolidRect.fs");
	m_Shader_Lecture3SimpleParticle = CompileShaders("./Shaders/Lecture3/SimpleParticle.vs", "./Shaders/Lecture3/SimpleParticle.fs");
	m_Shader_Lecture5SinAnim = CompileShaders("./Shaders/Lecture5/SinAnim.vs", "./Shaders/Lecture5/SinAnim.fs");
	m_Shader_Lecture6TextureSampling = CompileShaders("./Shaders/Lecture6/TextureSampling.vs", "./Shaders/Lecture6/TextureSampling.fs");

	//Create VBOs
	CreateVertexBufferObjects();
	GenRectsVBO(50000, 0.01f, false, true);
	GenCheckerBoardVBO();

	// Load Textures
	m_T_RGB = CreatePngTexture("./Textures/T_RGB.png");
}

void Renderer::CreateVertexBufferObjects()
{
	float rect[]
		=
	{
		-0.5, -0.5, 0.f, -0.5, 0.5, 0.f, 0.5, 0.5, 0.f, //Triangle1
		-0.5, -0.5, 0.f,  0.5, 0.5, 0.f, 0.5, -0.5, 0.f, //Triangle2
	};

	glGenBuffers(1, &m_VBORect);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);

}

void Renderer::GenCheckerBoardVBO()
{

	static const GLulong checkerboard[] =
	{
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF
	};

	glGenTextures(1, &m_T_CheckerBoard);
	glBindTexture(GL_TEXTURE_2D, m_T_CheckerBoard);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 8, 8, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkerboard);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	float size = 0.5f;
	float rect[] = 
	{
		// x, y, z, u, v
		// triangle1
		-size, -size, 0.f, 0.f, 0.f,
		-size, size, 0.f, 0.f, 1.f,
		size, size, 0.f, 1.f, 1.f,

		// triangle2
		-size, -size, 0.f, 0.f, 0.f,
		size, -size, 0.f, 1.f, 0.f,
		size, size, 0.f, 1.f, 1.f
	};

	glGenBuffers(1, &m_VBOTextureRect);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);
}

void Renderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
	//쉐이더 오브젝트 생성
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
	}

	const GLchar* p[1];
	p[0] = pShaderText;
	GLint Lengths[1];
	Lengths[0] = (GLint)strlen(pShaderText);
	//쉐이더 코드를 쉐이더 오브젝트에 할당
	glShaderSource(ShaderObj, 1, p, Lengths);

	//할당된 쉐이더 코드를 컴파일
	glCompileShader(ShaderObj);

	GLint success;
	// ShaderObj 가 성공적으로 컴파일 되었는지 확인
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];

		//OpenGL 의 shader log 데이터를 가져옴
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
		printf("%s \n", pShaderText);
	}

	// ShaderProgram 에 attach!!
	glAttachShader(ShaderProgram, ShaderObj);
}

bool Renderer::ReadFile(char* filename, std::string *target)
{
	std::ifstream file(filename);
	if (file.fail())
	{
		std::cout << filename << " file loading failed.. \n";
		file.close();
		return false;
	}
	std::string line;
	while (getline(file, line)) {
		target->append(line.c_str());
		target->append("\n");
	}
	return true;
}

GLuint Renderer::CompileShaders(char* filenameVS, char* filenameFS)
{
	GLuint ShaderProgram = glCreateProgram(); //빈 쉐이더 프로그램 생성

	if (ShaderProgram == 0) { //쉐이더 프로그램이 만들어졌는지 확인
		fprintf(stderr, "Error creating shader program\n");
	}

	std::string vs, fs;

	//shader.vs 가 vs 안으로 로딩됨
	if (!ReadFile(filenameVS, &vs)) {
		printf("Error compiling vertex shader\n");
		return -1;
	};

	//shader.fs 가 fs 안으로 로딩됨
	if (!ReadFile(filenameFS, &fs)) {
		printf("Error compiling fragment shader\n");
		return -1;
	};

	// ShaderProgram 에 vs.c_str() 버텍스 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);

	// ShaderProgram 에 fs.c_str() 프레그먼트 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	//Attach 완료된 shaderProgram 을 링킹함
	glLinkProgram(ShaderProgram);

	//링크가 성공했는지 확인
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (Success == 0) {
		// shader program 로그를 받아옴
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error linking shader program\n" << ErrorLog;
		return -1;
	}

	glValidateProgram(ShaderProgram);
	glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error validating shader program\n" << ErrorLog;
		return -1;
	}

	glUseProgram(ShaderProgram);
	std::cout << filenameVS << ", " << filenameFS << " Shader compiling is done.\n";

	return ShaderProgram;
}
unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight)
{
	std::cout << "Loading bmp file " << imagepath << " ... " << std::endl;
	outWidth = -1;
	outHeight = -1;
	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = NULL;
	fopen_s(&file, imagepath, "rb");
	if (!file)
	{
		std::cout << "Image could not be opened, " << imagepath << " is missing. " << std::endl;
		return NULL;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1E]) != 0)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1C]) != 24)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	outWidth = *(int*)&(header[0x12]);
	outHeight = *(int*)&(header[0x16]);

	if (imageSize == 0)
		imageSize = outWidth * outHeight * 3;

	if (dataPos == 0)
		dataPos = 54;

	data = new unsigned char[imageSize];

	fread(data, 1, imageSize, file);

	fclose(file);

	std::cout << imagepath << " is succesfully loaded. " << std::endl;

	return data;
}

GLuint Renderer::CreatePngTexture(char * filePath)
{
	//Load Pngs: Load file and decode image.
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, filePath);
	if (error != 0)
	{
		lodepng_error_text(error);
		assert(error == 0);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

	return temp;
}

GLuint Renderer::CreateBmpTexture(char * filePath)
{
	//Load Bmp: Load file and decode image.
	unsigned int width, height;
	unsigned char * bmp
		= loadBMPRaw(filePath, width, height);

	if (bmp == NULL)
	{
		std::cout << "Error while loading bmp file : " << filePath << std::endl;
		assert(bmp != NULL);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp);

	return temp;
}


void Renderer::GenRectsVBO(int RectCount, float RectSize, bool bRandPos, bool bRandColor)
{
	int countQuad = RectCount;
	float size = RectSize;

	int vertexPerQuad = 6;
	int floatsPerVertex = 3 + 3 + 4 + 1 + 4; // 위치(3), 속도(3), 진폭주기,생명주기(4), 고유값(1), 색상(4)

	int arraySize = countQuad * vertexPerQuad * floatsPerVertex;

	float *verticies = new float[arraySize];

	// Position
	float randX = 0.f, randY = 0.f;
	float Z = 0.f;

	// 속도
	float randVelX = 0.f, randVelY = 0.f;
	float VelZ = 0.f;

	// 생명주기
	float startTime, lifeTime;
	float startTimeMax = 3.f;
	float lifeTimeMin = 0.2f, lifeTimeMax = 0.5f;

	// 진폭, 주기
	float ratio, amplitude;
	float ratioMin = 0.5f, ratioThres = 1.f;				// 주기
	float amplitudeMin = 0.1f, amplitudeThres = 0.5f;	// 진폭
	int index = 0;

	// 고유 값
	float randValue = 0.f, randThres = 1.f;

	// 색깔
	float r, g, b, a;


	for (int i = 0; i < RectCount; i++)
	{
		if (bRandPos)
		{
			randX = (float(rand()) / float(RAND_MAX) - 0.5f) * 1.f;
			randY = (float(rand()) / float(RAND_MAX) - 0.5f) * 1.f;
		}
		else
		{
			randX = 0.f;
			randY = 0.f;
		}

		randVelX = (float(rand()) / float(RAND_MAX) - 0.5f) * 0.5f;
		randVelY = (float(rand()) / float(RAND_MAX) - 0.5f) * 0.5f;

		startTime = (float(rand() / float(RAND_MAX))) * startTimeMax;
		lifeTime = (float(rand() / float(RAND_MAX))) * lifeTimeMax + lifeTimeMin;
		ratio = (float(rand() / float(RAND_MAX))) * ratioThres + ratioMin;
		amplitude = (float(rand() / float(RAND_MAX))) * amplitudeThres + amplitudeMin;

		randValue = randValue + ((float)rand() / (float)RAND_MAX) * randThres;

		if (bRandColor)
		{
			r = (float(rand() / float(RAND_MAX)));
			g = (float(rand() / float(RAND_MAX)));
			b = (float(rand() / float(RAND_MAX)));
			a = (float(rand() / float(RAND_MAX)));
		}
		else
		{
			r = 1.f;
			g = 1.f;
			b = 1.f;
			a = 1.f;
		}

		index = i * floatsPerVertex * vertexPerQuad;

		// Triangle1
		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;
		verticies[index] = randVelX; index++;
		verticies[index] = randVelY; index++;
		verticies[index] = VelZ; index++;
		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = amplitude; index++;
		verticies[index] = ratio; index++;
		verticies[index] = randValue; index++;
		verticies[index] = r; index++;
		verticies[index] = g; index++;
		verticies[index] = b; index++;
		verticies[index] = a; index++;


		verticies[index] = randX + size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;
		verticies[index] = randVelX; index++;
		verticies[index] = randVelY; index++;
		verticies[index] = VelZ; index++;
		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = amplitude; index++;
		verticies[index] = ratio; index++;
		verticies[index] = randValue; index++;
		verticies[index] = r; index++;
		verticies[index] = g; index++;
		verticies[index] = b; index++;
		verticies[index] = a; index++;

		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;
		verticies[index] = randVelX; index++;
		verticies[index] = randVelY; index++;
		verticies[index] = VelZ; index++;
		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = amplitude; index++;
		verticies[index] = ratio; index++;
		verticies[index] = randValue; index++;
		verticies[index] = r; index++;
		verticies[index] = g; index++;
		verticies[index] = b; index++;
		verticies[index] = a; index++;


		// ---

		// triangle2
		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;
		verticies[index] = randVelX; index++;
		verticies[index] = randVelY; index++;
		verticies[index] = VelZ; index++;
		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = amplitude; index++;
		verticies[index] = ratio; index++;
		verticies[index] = randValue; index++;
		verticies[index] = r; index++;
		verticies[index] = g; index++;
		verticies[index] = b; index++;
		verticies[index] = a; index++;

		verticies[index] = randX - size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;
		verticies[index] = randVelX; index++;
		verticies[index] = randVelY; index++;
		verticies[index] = VelZ; index++;
		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = amplitude; index++;
		verticies[index] = ratio; index++;
		verticies[index] = randValue; index++;
		verticies[index] = r; index++;
		verticies[index] = g; index++;
		verticies[index] = b; index++;
		verticies[index] = a; index++;

		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;
		verticies[index] = randVelX; index++;
		verticies[index] = randVelY; index++;
		verticies[index] = VelZ; index++;
		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = amplitude; index++;
		verticies[index] = ratio; index++;
		verticies[index] = randValue; index++;
		verticies[index] = r; index++;
		verticies[index] = g; index++;
		verticies[index] = b; index++;
		verticies[index] = a; index++;

	}

	glGenBuffers(1, &m_VBORandRects);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORandRects);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * index, verticies, GL_STATIC_DRAW);

	m_GenRandRects_VertexCount = vertexPerQuad * countQuad;
}

void Renderer::Test()
{
	glUseProgram(m_Shader_SolidRect);

	int attribPosition = glGetAttribLocation(m_Shader_SolidRect, "a_Position");
	glEnableVertexAttribArray(attribPosition);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(attribPosition);
}


void Renderer::Lecture3_SimpleParticle()
{
	GLuint shader = m_Shader_Lecture3SimpleParticle;
	glUseProgram(shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01f;

	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	GLuint aVel = glGetAttribLocation(shader, "a_Vel");
	GLuint aStartLifeAmpRatio = glGetAttribLocation(shader, "a_StartLifeAmpRatio");
	GLuint aRandValue = glGetAttribLocation(shader, "a_RandValue");
	GLuint aColor = glGetAttribLocation(shader, "a_Color");


	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aVel);
	glEnableVertexAttribArray(aStartLifeAmpRatio);
	glEnableVertexAttribArray(aRandValue);
	glEnableVertexAttribArray(aColor);



	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, 0);
	glVertexAttribPointer(aVel, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 3));
	glVertexAttribPointer(aStartLifeAmpRatio, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 6));
	glVertexAttribPointer(aRandValue, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 10));
	glVertexAttribPointer(aColor, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 11));




	glBindBuffer(GL_ARRAY_BUFFER, m_VBORandRects);
	glDrawArrays(GL_TRIANGLES, 0, m_GenRandRects_VertexCount);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aVel);
	glDisableVertexAttribArray(aStartLifeAmpRatio);
	glDisableVertexAttribArray(aRandValue);
	glDisableVertexAttribArray(aColor);



}

void Renderer::Lecture5_FS_SinAnim()
{
	GLuint shader = m_Shader_Lecture5SinAnim;
	glUseProgram(shader);

	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	glEnableVertexAttribArray(aPos);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.005f;

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(aPos);
}

void Renderer::Lecture6_TextureSampling()
{
	GLuint shader = m_Shader_Lecture6TextureSampling;
	glUseProgram(shader);

	// set Uniforms
	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.005f;

	// set Textures
	GLuint uTexture = glGetUniformLocation(shader, "u_Texture");
	glUniform1i(uTexture, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_T_RGB);

	// set Attributes
	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	GLuint aTex = glGetAttribLocation(shader, "a_TexPos");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTex);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTex, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));


	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTex);

}

