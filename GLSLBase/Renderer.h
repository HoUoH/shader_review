#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"
#include "Dependencies\wglew.h"
#include "Dependencies\glm/glm.hpp"
#include "Dependencies\glm/gtc/matrix_transform.hpp"
#include "Dependencies\glm/gtx/euler_angles.hpp"

class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();

	GLuint CreatePngTexture(char * filePath);
	GLuint CreateBmpTexture(char * filePath);
	   
	void Test();
	void GenRectsVBO(int RectCount, float RectSize, bool bRandPos, bool bRandColor);

	void Lecture3_SimpleParticle();
	void Lecture5_FS_SinAnim();
	void Lecture6_TextureSampling();

	// m_Var
	int m_GenRandRects_VertexCount = 0;

private:
	void Initialize(int windowSizeX, int windowSizeY);
	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects(); 
	void GenCheckerBoardVBO();

	unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight);

	bool m_Initialized = false;
	
	unsigned int m_WindowSizeX = 0;
	unsigned int m_WindowSizeY = 0;

	// VBO
	GLuint m_VBORect = 0;
	GLuint m_VBORandRects = 0;
	GLuint m_VBOTextureRect = 0;

	// Shader 
	GLuint m_Shader_SolidRect = 0;
	GLuint m_Shader_Lecture3SimpleParticle = 0;
	GLuint m_Shader_Lecture5SinAnim = 0;
	GLuint m_Shader_Lecture6TextureSampling = 0;

	// Textures
	GLuint m_T_CheckerBoard = 0;
	GLuint m_T_RGB = 0;


	
};

