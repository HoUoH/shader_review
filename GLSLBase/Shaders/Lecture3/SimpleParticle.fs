#version 450

layout(location=0) out vec4 FragColor;

in vec2 v_OriXY;
in float v_Radius;
in vec4 v_Color;

void main()
{
	float dis = length(v_OriXY);
	vec4 newColor;
	if (dis > v_Radius)
	{
		newColor = vec4(0, 0, 0, 0);
	}
	else
	{
		newColor = v_Color;
		newColor.a = pow(1-dis/v_Radius, 1);
	}
	

	FragColor = newColor;
}
