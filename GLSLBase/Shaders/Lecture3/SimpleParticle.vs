#version 450

in vec3 a_Position;
in vec3 a_Vel;
in vec4 a_StartLifeAmpRatio;
in float a_RandValue;
in vec4 a_Color;


uniform float u_Time;
const mat3 c_RP = mat3(0, -1, 0,  1, 0, 0,  0, 0, 0);
const float PI = 3.141592;
const vec3 c_Gravity = vec3(0, -9.8, 0);

out vec2 v_OriXY;
out float v_Radius;
out vec4 v_Color;

void main()
{

	// 윈도우 중앙에서 오른쪽으로 발사 (무작위 속도로 발사)
	/*
	vec3 newPos = a_Position;
	vec2 newVel = a_Vel.xy;
	newPos.x += newVel.x * u_Time;
	newPos.y += newVel.y * u_Time;

	gl_Position = vec4(newPos, 1);
	*/

	// 파티클 움직임이 Sin 곡선 따라가게
	/*
	vec3 newPos = a_Position;
	newPos.x += u_Time;
	newPos.y += sin(2 * PI * u_Time) / 2;

	gl_Position = vec4(newPos,1 );
	*/

	// Sin 무작위 진폭, 주기
	/*
	vec3 newPos = a_Position;
	newPos.x += u_Time;
	newPos.y += sin(2 * PI * u_Time * a_StartLifeAmpRatio.w) / a_StartLifeAmpRatio.z;
	gl_Position = vec4(newPos,1 );
	*/
	
	// Emit 후 점점 퍼지게 + 원 둘레로

	

	vec3 newPos = a_Position.xyz;
	float ElapsedTime = u_Time;
	float StartTime = a_StartLifeAmpRatio.x;
	float LifeTime = a_StartLifeAmpRatio.y;
	float Amp = a_StartLifeAmpRatio.z;
	float Ratio = a_StartLifeAmpRatio.w;

	float SpawnElapsedTime = ElapsedTime - StartTime;
	float Life = LifeTime - SpawnElapsedTime;
	float newAlpha;
	
	if (SpawnElapsedTime > 0)
	{
		SpawnElapsedTime = mod(SpawnElapsedTime, LifeTime);

		newPos.x += cos(a_RandValue * 2 * PI);
		newPos.y += sin(a_RandValue * 2 * PI);

		// 중력가속도 포함
		//newPos = newPos + a_Vel * SpawnElapsedTime + (0.5 * c_Gravity * SpawnElapsedTime * SpawnElapsedTime);
		
		// 중력가속도 미포함
		newPos = newPos + a_Vel * SpawnElapsedTime;


		vec3 vSin = a_Vel * c_RP;
		newPos = newPos + vSin * sin(SpawnElapsedTime * PI * 2 * Ratio) * Amp;

		newAlpha = pow(1 - SpawnElapsedTime / LifeTime, 1);
	}
	else
	{
		newPos = vec3(100000, 100000, 100000);
	}
	gl_Position = vec4(newPos, 1);

	v_OriXY = a_Position.xy;
	v_Radius = abs(a_Position.x);
	v_Color = a_Color;
	v_Color.a = newAlpha;
	

}
