#version 450

layout(location=0) out vec4 FragColor;

uniform sampler2D u_Texture;

in vec2 v_TexPos;

void main()
{
	vec4 newColor;

	// 연습
	// 34P 첫번째 그림 내 식
	/*
	vec2 newTexPos = v_TexPos;
	newTexPos.y = abs(newTexPos.y - 0.5f) * 2;

	newColor = texture(u_Texture, newTexPos);
	*/

	// 34P 첫번째 그림
	/*
	vec2 newTexPos = abs(v_TexPos - vec2(0, 0.5)) + vec2(0, 0.5);
	newTexPos.y *= 2;

	newColor = texture(u_Texture, newTexPos);
	*/

	// 34P 두번째 그림
	// UV 좌표 연산 안에서 v_TexPos.x 와 v_TexPos.y의 값이 범위가 같더라도
	// 연산이 다르게 일어난다. 그러니까, v_TexPos.x 에서 x,y성분이 있고, v_TexPos.y 에서 x,y성분이 있다고 생각
	/*
	vec2 newTexPos;
	newTexPos.x = v_TexPos.x * 3.0;
	newTexPos.y = v_TexPos.y / 3.0;
	newTexPos.y += floor(v_TexPos.x * 3) / 3.0;

	newColor = texture(u_Texture, newTexPos);
	*/

	// 34P 세번째 그림
	/*
	vec2 newTexPos;
	// ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★
	// 단순 연산은 범위 설정이라 생각.
	// 더하기는 변화량을 추가하는것. (자기자신)
	// fract는 소수점 부분을 원할 때 사용 / 0~1인거에 3을 곱하고 fract를 취하면 0~1, 0~1, 0~1 값 얻음
	// floor는, 해당 정수를 원할 때 사용 / 0~1인거에 3을 곱하고 floor를 취하면 0, 1, 2의 값 얻음
	// ★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★

	newTexPos.x = v_TexPos.x * 3;
	newTexPos.y = v_TexPos.y / 3.0;
	newTexPos.y += (2 - floor(v_TexPos.x * 3.0)) / 3.0;

	newColor = texture(u_Texture, newTexPos);
	*/

	// 34P 네번째 그림
	vec2 newTexPos;
	newTexPos.x = v_TexPos.x;
	newTexPos.y = (2 -floor(v_TexPos.y * 3.0)) / 3.0;
	newTexPos.y += fract(v_TexPos.y * 3.0) / 3.0;


	newColor = texture(u_Texture, newTexPos);

	FragColor = newColor;
}
