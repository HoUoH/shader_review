#version 450

layout(location=0) out vec4 FragColor;

const float PI = 3.141592;

in vec2 v_OriXY;

uniform float u_Time;

void main()
{
	vec2 texPos = v_OriXY;
	vec4 newColor;

	texPos.x += u_Time;

	float sinValue = sin(texPos.x * 2 * PI) * 0.5 + 0.5;
	float thickness = 0.01f;

	if (texPos.y > sinValue && texPos.y < sinValue + thickness)
	{
		newColor = vec4(1,1,1,1);
	}
	else
	{
		newColor = vec4(0,0,0,0);
	}

	FragColor = newColor;
}
