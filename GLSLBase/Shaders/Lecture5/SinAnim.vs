#version 450

in vec3 a_Position;

out vec2 v_OriXY;

void main()
{
	gl_Position = vec4(a_Position, 1);

	v_OriXY = a_Position.xy + 0.5f;
}
